﻿using System;
using System.Runtime.InteropServices;
using BindingSupport;
using static NuklearSharp.TypeDefinition;

namespace NuklearSharp
{
    public class Context : DLSupport
    {
        #region Delegate Variables

        public nk_init_default m_nk_init_default; 
        public nk_init_fixed m_nk_init_fixed;
        public nk_init m_nk_init;
        public nk_init_custom m_nk_init_custom;
        public nk_clear m_nk_clear;
        public nk_free m_nk_free;
        public nk_set_user_data m_nk_set_user_data;

        public nk_input_begin m_nk_input_begin;
        public nk_input_motion m_nk_input_motion;
        public nk_input_key m_nk_input_key;
        public nk_input_scroll m_nk_input_scroll;
        public nk_input_char m_nk_input_char;
        public nk_input_glyph m_nk_input_glyph;
        public nk_input_unicode m_nk_input_unicode;
        public nk_input_end m_nk_input_end;
        
        public nk_begin m_nk_begin;
        public nk_begin_titled m_nk_begin_titled;
        public nk_end m_nk_end;
        
        #endregion
        
        
        public Context() : base("libNuklear.so", LoadFlag.RTLD_NOW)
        {
            m_nk_init_default = GetFunctionDelegate<nk_init_default> ("nk_init_default");
            m_nk_init_fixed = GetFunctionDelegate<nk_init_fixed> ("nk_init_fixed");
            m_nk_init = GetFunctionDelegate<nk_init> ("nk_init");
            m_nk_init_custom = GetFunctionDelegate<nk_init_custom> ("nk_init_custom");
            m_nk_clear = GetFunctionDelegate<nk_clear> ("nk_clear");
            m_nk_free = GetFunctionDelegate<nk_free> ("nk_free");
            m_nk_set_user_data = GetFunctionDelegate<nk_set_user_data> ("nk_set_user_data");
            m_nk_begin = GetFunctionDelegate<nk_begin> ("nk_begin");
            m_nk_begin_titled = GetFunctionDelegate<nk_begin_titled> ("nk_begin_titled");
            m_nk_end = GetFunctionDelegate<nk_end> ("nk_end");
            m_nk_input_begin = GetFunctionDelegate<nk_input_begin>("nk_input_begin");
            m_nk_input_motion = GetFunctionDelegate<nk_input_motion>("nk_input_motion");
            m_nk_input_key = GetFunctionDelegate<nk_input_key>("nk_input_key");
            m_nk_input_scroll = GetFunctionDelegate<nk_input_scroll>("nk_input_scroll");
            m_nk_input_char = GetFunctionDelegate<nk_input_char>("nk_input_char");
            m_nk_input_glyph = GetFunctionDelegate<nk_input_glyph>("nk_input_glyph");
            m_nk_input_unicode = GetFunctionDelegate<nk_input_unicode>("nk_input_unicode");
            m_nk_input_end = GetFunctionDelegate<nk_input_end>("nk_input_end");
        }

        #region Context

        public delegate int nk_init_default(IntPtr context, IntPtr font);
        public delegate int nk_init_fixed(IntPtr context, IntPtr memory, UIntPtr size, IntPtr font);
        public delegate int nk_init(IntPtr context, IntPtr allocator, IntPtr font);
        public delegate int nk_init_custom(IntPtr context, IntPtr cmds, IntPtr pool, IntPtr font);
        public delegate void nk_clear(IntPtr context);
        public delegate void nk_free(IntPtr context);
        public delegate void nk_set_user_data(IntPtr context, nk_handle handle);
        
        #endregion

        #region Input

        public delegate void nk_input_begin(IntPtr context);
        public delegate void nk_input_motion(IntPtr context, int x, int y);
        public delegate void nk_input_key(IntPtr context, Keys key, int down);
        public delegate void nk_input_scroll(IntPtr context, Vector2 val);
        public delegate void nk_input_char(IntPtr context, char c);
        public delegate void nk_input_glyph(IntPtr context, char glyph);
        public delegate void nk_input_unicode(IntPtr context, uint rune);
        public delegate void nk_input_end(IntPtr context);

        #endregion

        #region Drawing

        

        #endregion

        #region Window

        public delegate int nk_begin(IntPtr context, IntPtr panel, [MarshalAs(UnmanagedType.LPStr)]string title, IntPtr bounds, uint flags);
        public delegate int nk_begin_titled(IntPtr context, IntPtr panel, [MarshalAs(UnmanagedType.LPStr)] string name, [MarshalAs(UnmanagedType.LPStr)] string title, IntPtr bounds, uint flags);
        public delegate void nk_end(IntPtr context);
        public delegate IntPtr nk_window_find(IntPtr context, [MarshalAs(UnmanagedType.LPStr)] string name);
        public delegate Rect nk_window_get_bounds(IntPtr context);
        public delegate Vector2 nk_window_get_position(IntPtr context);

        #endregion
    }
}