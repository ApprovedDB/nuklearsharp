﻿namespace NuklearSharp
{
    public enum ButtonBehaviour : uint
    {
        NK_BUTTON_DEFAULT,
        NK_BUTTON_REPEATER
    }
}