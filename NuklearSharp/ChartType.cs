﻿namespace NuklearSharp
{
    public enum ChartType : uint
    {
        NK_CHART_LINES,
        NK_CHART_COLUMN,
        NK_CHART_MAX
    }
}