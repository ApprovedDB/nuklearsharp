﻿using System;
using System.Runtime.InteropServices;

namespace NuklearSharp
{
    public sealed class TypeDefinition
    {
        public int NK_FLAG(int offset)
        {
            return (1 << (offset));
        }
        
        [StructLayout(LayoutKind.Sequential)]
        public struct nk_handle
        {
            public IntPtr ptr;
            public int id;
        }

        public delegate IntPtr nk_plugin_alloc(nk_handle handle, IntPtr old, UIntPtr size);
        public delegate void nk_plugin_free(nk_handle handle, IntPtr old);
        public delegate int nk_plugin_filter(IntPtr nk_text_edit, uint unicode);

        public delegate void nk_plugin_paste(nk_handle handle, IntPtr nk_text_edit);

        public delegate void nk_plugin_copy(nk_handle handle, IntPtr c, int length);
    }
}