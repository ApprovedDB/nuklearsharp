﻿namespace NuklearSharp
{
    public enum Modify : uint
    {
        NK_FIXED, 
        NK_MODIFIABLE
    }
}