﻿namespace NuklearSharp
{
    public enum AntiAlias : uint
    {
        NK_ANTI_ALIASING_OFF,
        NK_ANTI_ALIASING_ON
    }
}