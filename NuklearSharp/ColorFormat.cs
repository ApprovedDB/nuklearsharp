﻿namespace NuklearSharp
{
    public enum ColorFormat : uint
    {
        NK_RGB,
        NK_RGBA
    }
}