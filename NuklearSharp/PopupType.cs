﻿namespace NuklearSharp
{
    public enum PopupType : uint
    {
        NK_POPUP_STATIC,
        NK_POPUP_DYNAMIC
    }
}