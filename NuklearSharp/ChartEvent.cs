﻿namespace NuklearSharp
{
    public enum ChartEvent : uint
    {
        NK_CHART_HOVERING = 0x01,
        NK_CHART_CLICKED = 0x02
    }
}