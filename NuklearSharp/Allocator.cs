﻿using System.Runtime.InteropServices;
using static NuklearSharp.TypeDefinition;

namespace NuklearSharp
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Allocator
    {
        public nk_handle UserData { get; set; }
        public nk_plugin_alloc Alloc { get; set; }
        public nk_plugin_free Free { get; set; }

        public Allocator(nk_handle userData, nk_plugin_alloc alloc, nk_plugin_free free)
        {
            UserData = userData;
            Alloc = alloc;
            Free = free;
        }
    }
}