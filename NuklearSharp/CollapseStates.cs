﻿namespace NuklearSharp
{
    public enum CollapseStates : uint
    {
        NK_MINIMIZED,
        NK_MAXIMIZED
    }
}