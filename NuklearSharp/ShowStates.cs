﻿namespace NuklearSharp
{
    public enum ShowStates : uint
    {
        NK_HIDDEN,
        NK_SHOWN
    }
}