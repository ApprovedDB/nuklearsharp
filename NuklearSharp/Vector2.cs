﻿using System.Runtime.InteropServices;

namespace NuklearSharp
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Vector2
    {
        public float X { get; set; }
        public float Y { get; set; }

        public Vector2(float x, float y)
        {
            X = x;
            Y = y;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Vector2i
    {
        public short X { get; set; }
        public short Y { get; set; }

        public Vector2i(short x, short y)
        {
            X = x;
            Y = y;
        }
    }
}