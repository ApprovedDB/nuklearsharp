﻿using System;
using System.Runtime.InteropServices;

namespace NuklearSharp
{
    [StructLayout(LayoutKind.Sequential)]
    public struct ListView
    {
        public int Count { get; set; }

        private int TotalHeight;
        private IntPtr context;
        private IntPtr scroll_pointer;
        private uint scroll_value;

        public int Begin(IntPtr context, out ListView listView, [MarshalAs(UnmanagedType.LPStr)] string id, uint flags,
            int row_heights, int row_count)
        {
            //TODO fix
            listView = this;
            return 0;
        }

        public void End(ListView listView)
        {
            //TODO fix
        }
    }
}