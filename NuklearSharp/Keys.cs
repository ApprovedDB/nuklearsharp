﻿using System;

namespace NuklearSharp
{
    public enum Keys : uint
    {
        NK_KEY_NONE,
        NK_KEY_SHIFT,
        NK_KEY_CTRL,
        NK_KEY_DEL,
        NK_KEY_ENTER,
        NK_KEY_TAB,
        NK_KEY_BACKSPACE,
        NK_KEY_COPY,
        NK_KEY_CUT,
        NK_KEY_PASTE,
        NK_KEY_UP,
        NK_KEY_DOWN,
        NK_KEY_LEFT,
        NK_KEY_RIGHT,
        
        /// <summary>
        /// Shortcuts : text field
        /// </summary>
        NK_KEY_TEXT_INSERT_MODE,
        NK_KEY_TEXT_REPLACE_MODE,
        NK_KEY_TEXT_RESET_MODE,
        NK_KEY_TEXT_LINE_START,
        NK_KEY_TEXT_LINE_END,
        NK_KEY_TEXT_START,
        NK_KEY_TEXT_END,
        NK_KEY_TEXT_UNDO,
        NK_KEY_TEXT_REDO,
        NK_KEY_TEXT_SELECT_ALL,
        NK_KEY_TEXT_WORD_LEFT,
        NK_KEY_TEXT_WORD_RIGHT,
        
        /// <summary>
        /// Shortcuts: scrollbar
        /// </summary>
        NK_KEY_SCROLL_START,
        NK_KEY_SCROLL_END,
        NK_KEY_SCROLL_DOWN,
        NK_KEY_SCROLL_UP,
        NK_KEY_MAX
    }
}