﻿namespace NuklearSharp
{
    public enum PanelFlags : uint
    {
        NK_WINDOW_BORDER            = 1, /* Draws a border around the window to visually separate window from the background */
        NK_WINDOW_MOVABLE           = 2, /* The movable flag indicates that a window can be moved by user input or by dragging the window header */
        NK_WINDOW_SCALABLE          = 4, /* The scalable flag indicates that a window can be scaled by user input by dragging a scaler icon at the button of the window */
        NK_WINDOW_CLOSABLE          = 8, /* adds a closable icon into the header */
        NK_WINDOW_MINIMIZABLE       = 16, /* adds a minimize icon into the header */
        NK_WINDOW_NO_SCROLLBAR      = 32, /* Removes the scrollbar from the window */
        NK_WINDOW_TITLE             = 64, /* Forces a header at the top at the window showing the title */
        NK_WINDOW_SCROLL_AUTO_HIDE  = 128, /* Automatically hides the window scrollbar if no user interaction: also requires delta time in `nk_context` to be set each frame */
        NK_WINDOW_BACKGROUND        = 256, /* Always keep window in the background */
        NK_WINDOW_SCALE_LEFT        = 512, /* Puts window scaler in the left-ottom corner instead right-bottom*/
        NK_WINDOW_NO_INPUT          = 1024 /* Prevents window of scaling, moving or getting focus */
    }
}