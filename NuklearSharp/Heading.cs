﻿namespace NuklearSharp
{
    public enum Heading
    {
        NK_UP,
        NK_RIGHT,
        NK_DOWN,
        NK_LEFT
    }
}