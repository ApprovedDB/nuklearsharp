﻿using System.Dynamic;
using System.Runtime.InteropServices;

namespace NuklearSharp
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Rect
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float W { get; set; }
        public float H { get; set; }
        
        public Rect(float x, float y, float w, float h)
        {
            X = x;
            Y = y;
            W = w;
            H = h;
        }
        
        public void Set(float x, float y, float w, float h)
        {
            X = x;
            Y = y;
            W = w;
            H = h;
        }
    }
    
    [StructLayout(LayoutKind.Sequential)]
    public struct Recti
    {
        public short X { get; set; }
        public short Y { get; set; }
        public short W { get; set; }
        public short H { get; set; }
        
        public Recti(short x, short y, short w, short h)
        {
            X = x;
            Y = y;
            W = w;
            H = h;
        }
        
        public void Set(short x, short y, short w, short h)
        {
            X = x;
            Y = y;
            W = w;
            H = h;
        }
    }
}