﻿namespace NuklearSharp
{
    public enum LayoutFormat : uint
    {
        NK_DYNAMIC,
        NK_STATIC
    }
}