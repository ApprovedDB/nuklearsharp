﻿namespace NuklearSharp
{
    public enum Orientation : uint
    {
        NK_VERTICAL,
        NK_HORIZONTAL
    }
}