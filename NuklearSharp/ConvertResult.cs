﻿using static NuklearSharp.TypeDefinition;

namespace NuklearSharp
{
    public enum ConvertResult : uint
    {
        NK_CONVERT_SUCCESS = 0,
        NK_CONVERT_INVALID_PARAM = 1,
        NK_CONVERT_COMMAND_BUFFER_FULL = 2,
        NK_CONVERT_VERTEX_BUFFER_FULL = 4,
        NK_CONVERT_ELEMENT_BUFFER_FULL = 8
    }
}