﻿using System;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Reflection.Emit;

namespace BindingSupport
{
	/// <summary>
	/// Alternative to P/Invoke and allows any symbols to be retrieved through this method whereas P/Invoke may/may not be able to.
	/// </summary>
	public abstract class DLSupport : IDisposable
	{
		private bool IsDisposed = false;
		public bool SuppressSymbolError = false;
		internal IntPtr handle;


		[DllImport("dl")]
		public static extern IntPtr dlopen([MarshalAs(UnmanagedType.LPTStr)] string filename, int flags);

		[DllImport("dl")]
		public static extern IntPtr dlsym(IntPtr handle, [MarshalAs(UnmanagedType.LPTStr)] string symbol);

		[DllImport("dl")]
		public static extern int dlclose(IntPtr handle);

		protected DLSupport (string lib, LoadFlag flag)
		{
			AssemblyBuilder assembly = AppDomain.CurrentDomain.DefineDynamicAssembly (
				new AssemblyName ("DelegateTypeFactory"), AssemblyBuilderAccess.RunAndCollect);
			handle = dlopen (lib, (int)flag);
			if (handle == IntPtr.Zero)
				throw new DllNotFoundException ("Library cannot be found or loaded.");
		}

		protected T GetFunctionDelegate<T> (string symbol)
		{
			IntPtr ptr = dlsym (handle, symbol);
			if (!SuppressSymbolError && ptr == IntPtr.Zero)
				throw new Exception ($"Symbol cannot be found for type: {typeof(T).Name}");
			return (T)(object)Marshal.GetDelegateForFunctionPointer(ptr, typeof(T));
		}

		protected IntPtr GetSymbolPointer (string symbol)
		{
			return dlsym (handle, symbol);
		}

		public void Dispose ()
		{
			if (IsDisposed)
				return;
			IsDisposed = true;
			dlclose (handle);
		}
	}

	public enum LoadFlag : int
	{
		RTLD_LAZY = 0x00001,
		/// <summary>
		/// Immediate function call binding.
		/// </summary>
		RTLD_NOW = 0x00002,
		/// <summary>
		/// Mask of binding time value.
		/// </summary>
		RTLD_BINDING_MASK = 0x3,
		/// <summary>
		/// Do not load the object.
		/// </summary>
		RTLD_NOLOAD = 0x00004,
		/// <summary>
		/// Use deep binding.
		/// </summary>
		RTLD_DEEPBIND = 0x00008,

		/// <summary>
		/// If the following bit is set in the MODE argument to `dlopen',
		/// the symbols of the loaded object and its dependencies are made
		/// visible as if the object were linked directly into the program.
		/// </summary>
		RTLD_GLOBAL = 0x00100,

		/// <summary>
		/// Unix98 demands the following flag which is the inverse to RTLD_GLOBAL.
		/// The implementation does this by default and so we can define the
		/// value to zero.
		/// </summary>
		RTLD_LOCAL = 0,

		/// <summary>
		/// Do not delete object when closed.
		/// </summary>
		RTLD_NODELETE = 0x01000
	}
}
